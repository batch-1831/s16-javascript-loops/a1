// #1 to #7
let inputNum = parseInt(prompt("Give a number: "));
console.log("The number you provided is " +inputNum +".");

for(inputNum; inputNum >= 0  ; inputNum--) {
    if(inputNum <= 50) {
        console.log("The current value is at " + inputNum + ". Terminating the loop");
        break;
    }
    else if(inputNum % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number.");
    }
    else if (inputNum % 5 == 0) {
        console.log(inputNum);
    }
}

// #8 to #12
let longString = "supercalifragilisticexpialidocious";
let consonants = '';

console.log(longString);

for(x = 0; x < longString.length; x++) {
    if (longString[x] == 'a' || longString[x] == 'e' || longString[x] == 'i' || longString[x] == 'o' || longString[x] == 'u') {
        continue;
    }
    else {
        consonants += longString[x];
    }
}

console.log(consonants);